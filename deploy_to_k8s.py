# scripts/deploy_to_k8s.py
from kubernetes import client, config

def deploy_to_k8s(image_name, deployment_name):
    try:
        config.load_kube_config()
        v1 = client.AppsV1Api()

        # Define deployment
        deployment = {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {"name": deployment_name},
            "spec": {
                "replicas": 1,
                "selector": {
                    "matchLabels": {"app": deployment_name}
                },
                "template": {
                    "metadata": {"labels": {"app": deployment_name}},
                    "spec": {
                        "containers": [
                            {
                                "name": deployment_name,
                                "image": image_name,
                                "ports": [{"containerPort": 5000}]
                            }
                        ]
                    }
                }
            }
        }

        # Create deployment
        v1.create_namespaced_deployment(namespace="default", body=deployment)

        print(f"Successfully deployed {deployment_name} to Kubernetes")
    except Exception as e:
        print(f"Error deploying to Kubernetes: {e}")

if __name__ == "__main__":
    deploy_to_k8s("reddykiram52/webapp:latest", "webapp-deployment")

