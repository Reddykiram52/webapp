FROM python:3.8-slim

WORKDIR /app

# Copy only the necessary files
COPY templates /app/templates
COPY app.py /app
COPY requirements.txt /app

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Expose port 5000
EXPOSE 5000

# Set environment variable
ENV NAME webapp

# Run the application
CMD ["python", "app.py"]

