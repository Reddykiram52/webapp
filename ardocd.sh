#!/bin/bash

# Command 1
kubectl port-forward svc/argocd-server -n argocd 8080:443

# Command 2
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d


# Command 3
kubectl patch svc argocd-server -n argocd -p '{"spec": {"ports": [{"name": "http", "port": 80, "nodePort": 30007}]}}'

# Command 4
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "NodePort", "ports": [{"name": "http", "nodePort": 30007, "port": 80}]}}'

chmod +x your_script.sh
